﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace StreamReaderDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private String docsPath;
        private String dataFile = "TextFile.txt";
        private String dataFolder = "//Documents//StreamReaderData/";
        private String filePath = "";


        public MainWindow()
        {
            InitializeComponent();
            // InitFolders();
        }

        private void InitFolders()
        {
            // get the application path (Not used by this example)
            string appPath = System.AppDomain.CurrentDomain.BaseDirectory;

            // get the User's Home folder path
            docsPath = (Environment.OSVersion.Platform == PlatformID.Unix ||
                   Environment.OSVersion.Platform == PlatformID.MacOSX)
                    ? Environment.GetEnvironmentVariable("HOME")
                    : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");

            // Check if Documents/StreamReaderData folder present, if not create one
            Directory.CreateDirectory(docsPath + dataFolder);

            // Set the file path
            filePath = docsPath + dataFolder + dataFile;
        }

        private void ReadButton_Click(object sender, RoutedEventArgs e)
        {
            // set path to text file
            //  docsPath =  path to user My Documents folder
            //  dataFolder = folder name for storing app data
            //  dataFile = name of file for retrieve/store data
            filePath = docsPath + dataFolder + dataFile;

            StreamReader reader = new StreamReader(filePath);

            using (reader)
            {
                while (reader.Peek() >= 0)
                {
                    String data = reader.ReadLine();

                    FileItemsList.Items.Add(data);
                } // end while more file data
            } // end using reader
            reader.Close();
        } // end read button

        private void InitFoldersButton_Click(object sender, RoutedEventArgs e)
        {
            InitFolders();
            DeployStarterData();
            InitFoldersButton.IsEnabled = false;
            InitFoldersButton.Visibility = Visibility.Hidden;
            // InitFoldersButton.Visibility = Visibility.Visible;
        }

        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            FileItemsList.Items.Clear();
        }



        private void DeployStarterData()
        {
            // Copy the starter text file from Resources into the User's
            // Documents/StreamReaderData folder...

            // TODO: How to do this?

            // if file does not exist:
            if (!File.Exists(filePath))
            {
                // Version 1 - Write straight to the file
                StreamWriter writer = new StreamWriter(filePath);
                writer.WriteLine("Alpha");
                writer.WriteLine("Beta");
                writer.WriteLine("Charlie");
                writer.WriteLine("Delta");
                writer.WriteLine("Echo");
                writer.WriteLine("Foxtrot");
                writer.WriteLine("Omega");
                writer.WriteLine("Phi");
                writer.WriteLine("Zulu");
                writer.Close();
            } // end file does not exist

        }


    }
}
